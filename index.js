/*
import EventEmitter from "events";
export default EventEmitter;
*/

export default class EventEmitter {
    constructor() {
        this.observers = {};
    }

    on(name, observer) {
        const observers = this.observers[name];
        if (observers)
            observers.add(observer);
        else
            this.observers[name] = new Set([observer]);
    }

    off(name, observer) {
        const observers = this.observers[name];
        if (observers) {
            if (observers.has(observer)) {
                observers.remove(observer);
                if (!observers.size)
                    delete this.observers[name];
            }
            else
                console.warn(`events: unknown observer in "${name}"`);
        }
        else
            console.warn(`events: unknown signal "${name}"`);
    }

    emit(name, ...args) {
        if (name === "*")
            throw new Error("invalid signal name: '*' is reserved");
        this._emit(name, ...args);
        this._emit("*", ...args);
    }

    _emit(name, ...args) {
        const observers = this.observers[name];
        if (observers) {
            for (const observer of observers) {
                try {
                    observer(...args);
                } catch (e) {
                    console.error(`events: observer exception in "${name}":`, e);
                }
            }
        }
    }
}
